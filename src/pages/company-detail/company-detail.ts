import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CompanyService} from "../../services/company.service";
import {LaunchNavigator} from "@ionic-native/launch-navigator";
import {LoginPage} from "../login/login";

/**
 * Generated class for the CompanyDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-company-detail',
  templateUrl: 'company-detail.html',
})
export class CompanyDetailPage {

  private id;
  private company;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public companyService : CompanyService,
    public launchNavigator : LaunchNavigator,
    public app : App) {
    this.id = this.navParams.get('id');

  }

  ionViewWillEnter(){
    this.companyService.getById(this.id)
      .subscribe(res => {
        this.company = res;
      }, err => {
        this.logout();
      });
  }

  logout(){
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }

  openInMaps(){
    this.launchNavigator.navigate([this.company.lat,  this.company.long]);
  }

}
