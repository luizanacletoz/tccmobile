import {Component, NgZone} from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";

import {AuthService} from "../../services/auth.service";
import {RegisterPage} from "../register/register";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private login =  {
    email : 'luizantoniojj@hotmail.com',
    password : 'secret123'
  };

  public validationErrors;

  private error = '';
  private loader : Loading;



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService : AuthService,
    public ngZone : NgZone,
    public loadingCtrl: LoadingController,
    public toastCtrl : ToastController) {
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  public Login(): void {
    this.presentLoading();
    this.authService.auth(this.login)
      .subscribe(res => {
          localStorage.setItem('token', JSON.stringify(res))

          this.authService.info()
            .subscribe(res2 => {
              if(res2.role == 'client'){
                this.dismissLoading();
                this.navCtrl.setRoot(TabsPage);
                localStorage.setItem('client', JSON.stringify(res2))
              }else{
                this.dismissLoading();

                this.presentToast('Somente clientes podem utilizar o aplicativo');
                localStorage.clear();
              }
            }, err2 => {
              this.dismissLoading();
              this.presentToast('Houve um problema ao se comunicar com o servidor');
            });
        }, err => {
        this.presentToast('Houve um problema ao se comunicar com o servidor');
        this.dismissLoading();
        }
      );
  }

  goToRegisterPage(){
    this.navCtrl.push(RegisterPage);
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }

}
