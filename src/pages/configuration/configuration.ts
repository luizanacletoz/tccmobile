import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {ChangePersonalDataPage} from "../change-personal-data/change-personal-data";
import {ChangePasswordPage} from "../change-password/change-password";

/**
 * Generated class for the ConfigurationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {


  private config = {
    rangeKm : 20
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public app : App) {
  }

  ionViewDidEnter(){
    let storageConfig = localStorage.getItem("config");
    if(!storageConfig){
      localStorage.setItem("config", JSON.stringify(this.config));
    }else{
      this.config = JSON.parse(storageConfig);
    }
  }

  ionViewDidLeave(){
    localStorage.removeItem("config");
    localStorage.setItem("config", JSON.stringify(this.config));
  }

  changePersonalDataPage(){
    this.navCtrl.push(ChangePersonalDataPage);
  }

  changePasswordPage(){
    this.navCtrl.push(ChangePasswordPage);
  }

  logout(){
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }

}
