import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {ClientService} from "../../services/client.service";
import {TabsPage} from "../tabs/tabs";
import {AuthService} from "../../services/auth.service";

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private user = {
    name : '',
    email : '',
    password : ''
  };

  private loader;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public clientService : ClientService,
    public loadingCtrl : LoadingController,
    public authService : AuthService,
    public toastCtrl : ToastController) {
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  sendData(){
    this.presentLoading();
    this.clientService
      .register(this.user)
      .subscribe(res => {
        localStorage.setItem('token', JSON.stringify(res));

        this.authService.info()
          .subscribe(res2 => {
            if(res2.role == 'client'){
              this.dismissLoading();
              this.navCtrl.setRoot(TabsPage);
              localStorage.setItem('client', JSON.stringify(res2))
            }else{
              this.dismissLoading();
              this.presentToast('Somente clientes podem utilizar o aplicativo');
              localStorage.clear();
            }
          }, err2 => {
            this.dismissLoading();
            this.presentToast('Houve um problema ao se comunicar com o servidor');
          });
      }, err => {
        this.dismissLoading();
        this.presentToast('Houve um problema ao se comunicar com o servidor');
      });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }

}
