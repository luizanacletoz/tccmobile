import { Component } from '@angular/core';

import {BudgetPage} from "../budget/budget";
import {ConfigurationPage} from "../configuration/configuration";
import {AuthService} from "../../services/auth.service";
import {NotificationPage} from "../notification/notification";
import {NotificationService} from "../../services/notification.service";
import {LoginPage} from "../login/login";
import {App, Events} from "ionic-angular";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = BudgetPage;
  tab3Root = ConfigurationPage;
  tab4Root = NotificationPage;

  public count : number;
  private notifications = [];

  private config = {
    rangeKm : 20
  };

  constructor(
    public authService : AuthService,
    public notificationService : NotificationService,
    public app : App,
    public events : Events) {

    this.events.subscribe('notifications-change-zero', () => {
      this.count = 0;
    });

  }

  ionViewDidEnter(){
    let storageConfig = localStorage.getItem("config");
    if(!storageConfig){
      localStorage.setItem("config", JSON.stringify(this.config));
    }else{
      this.config = JSON.parse(storageConfig);
    }

    this.notificationService
      .getAll()
      .subscribe(res => {
        this.notifications = res;
        this.count = this.notifications.length;
      }, err => {
        this.logout();
      });
  }

  logout(){
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }
}
