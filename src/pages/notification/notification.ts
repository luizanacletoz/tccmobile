import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {NotificationService} from "../../services/notification.service";
import {LoginPage} from "../login/login";
import {BudgetDetailPage} from "../budget-detail/budget-detail";

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  private notifications;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public notificationService : NotificationService,
    public toastCtrl : ToastController,
    public events : Events) {
  }

  ionViewWillEnter(){
    this.get();
    this.events.publish('notifications-change-zero', 0);
  }

  get(){
    this.notificationService.getAll()
      .subscribe(res => {
        this.notifications = res;
      }, err => {
        this.toastCtrl.create({
          message: 'Erro ao se comunicar com o servidor',
          duration: 3000
        });
        this.logout();
      });
  }

  detail(budgetId){
      this.navCtrl.push(BudgetDetailPage, { id : budgetId });
  }

  logout(){
    localStorage.clear();
    this.navCtrl.setRoot(LoginPage);
  }

}
