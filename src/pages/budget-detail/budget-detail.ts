import {Component} from '@angular/core';
import {
  IonicPage,
  Loading,
  LoadingController,
  ModalController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import {BudgetService} from "../../services/budget.service";
import {LoginPage} from "../login/login";
import {CompanyDetailPage} from "../company-detail/company-detail";

/**
 * Generated class for the BudgetDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-budget-detail',
  templateUrl: 'budget-detail.html',
})
export class BudgetDetailPage {

  private id : String;
  private budget;
  private isOpen;

  private showItems = true;
  private showResponses = false;
  private showCompanies = false;

  private loader : Loading;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public budgetService : BudgetService,
    public loadingCtrl : LoadingController,
    public toastCtrl : ToastController) {
    this.id = navParams.get('id');
  }

  ionViewWillEnter() {
    this.presentLoading('Carregando...');
    this.budgetService.getById(this.id)
      .subscribe(res => {

        this.budget = res;
        this.isOpen = res.isOpen;
        this.dismissLoading();

      }, err => {
        this.logout();
      });
  }

  logout(){
    this.dismissLoading();
    localStorage.clear();
    this.navCtrl.setRoot(LoginPage);
  }

  toggleItems(){
    if(this.budget.items.length <= 0)
      return;

    this.showItems = !this.showItems;
    this.showResponses = false;
    this.showCompanies = false;
  }

  toggleResponses(){
    if(this.budget.responses.length <= 0)
      return;

    this.showResponses = !this.showResponses;
    this.showItems = false;
    this.showCompanies = false;
  }

  toggleCompanies(){
    if(this.budget.companies <= 0)
      return;

    this.showCompanies = !this.showCompanies;
    this.showItems = false;
    this.showResponses = false;
  }

  toggleBudget(){
    this.budget.isOpen = !this.isOpen;
    this.presentLoading('Um momento...');
    this.budgetService.toggle(this.id)
      .subscribe(res => {
        if(res.ok) {
          this.dismissLoading();
        }else {
          this.logout();
        }
      }, err => {
        this.toastCtrl.create({
          message: 'Erro ao se comunicar com o servidor',
          duration: 3000
        });
        this.logout();
      });
  }

  companyDetail(id){
    this.navCtrl.push(CompanyDetailPage, { id });
  }

  presentLoading(message) {
    this.loader = this.loadingCtrl.create({
      content: message
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }
}
