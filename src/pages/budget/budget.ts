import { Component } from '@angular/core';
import {IonicPage, Loading, LoadingController, ModalController, NavController, NavParams} from 'ionic-angular';
import {BudgetService} from "../../services/budget.service";
import {BudgetDetailPage} from "../budget-detail/budget-detail";
import {LoginPage} from "../login/login";
import {SearchOptionPage} from "../search-option/search-option";
import {CreateBudgetPage} from "../create-budget/create-budget";

/**
 * Generated class for the BudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-budget',
  templateUrl: 'budget.html',
})
export class BudgetPage {


  private budgets = [];

  private searchOptions = {
    isOpen : true
  };

  private loader : Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public budgetService : BudgetService,
    public loadingCtrl: LoadingController,
    public modalCtrl : ModalController) {
  }

  ionViewWillEnter(){
    this.getItems();
  }

  getItems(event = null){
    this.presentLoading();
    this.budgetService.getAll(
      this.searchOptions.isOpen
    ).subscribe(res => {
        this.budgets = res;
        this.dismissLoading();
        if(event){
          event.complete();
        }
      }, err => {
        this.presentLoading();
        localStorage.clear();
        this.navCtrl.setRoot(LoginPage);
      });
  }

  presentSearchOptionsModal() {
    let searchOptionsModal = this.modalCtrl.create(SearchOptionPage);
    searchOptionsModal.onDidDismiss(data => {
      if(typeof data !== 'undefined'){
        this.searchOptions = data;
        this.getItems();
      }
    });

    searchOptionsModal.present();
  }

  presentCreateBudgetModal(){
    let createBudgetModal = this.modalCtrl.create(CreateBudgetPage);
    createBudgetModal.onDidDismiss(data => {
       if(typeof data !== 'undefined' && data.completed === true){
        this.detail(data.id);
      }
    });

    createBudgetModal.present();
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }

  detail(id){
    this.navCtrl.push(BudgetDetailPage, { id })
  }

}
