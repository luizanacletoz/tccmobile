import { Component } from '@angular/core';
import {App, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthService} from "../../services/auth.service";
import {LoginPage} from "../login/login";
import {ClientService} from "../../services/client.service";

/**
 * Generated class for the ChangePersonalDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-personal-data',
  templateUrl: 'change-personal-data.html',
})
export class ChangePersonalDataPage {

  private name : String;

  private loader

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService : AuthService,
    public app : App,
    public clientService : ClientService,
    public loadingCtrl : LoadingController) {

  }

  ionViewWillEnter() {
    this.authService.info()
      .subscribe(res => {
        this.name = res.name;
      }, err => {
        this.logout();
      });
  }

  changeData(){
    this.presentLoading();
    this.clientService
      .changeClientData({ name : this.name })
      .subscribe(res => {
        this.dismissLoading();
        this.navCtrl.pop();
      }, err => {
        this.dismissLoading();
        this.logout();
      });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Um momento..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }

  logout(){
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }

}
