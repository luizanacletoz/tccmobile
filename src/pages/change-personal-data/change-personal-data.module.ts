import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangePersonalDataPage } from './change-personal-data';

@NgModule({
  declarations: [
    ChangePersonalDataPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangePersonalDataPage),
  ],
})
export class ChangePersonalDataPageModule {}
