import { Component } from '@angular/core';
import {App, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AuthService} from "../../services/auth.service";
import {LoginPage} from "../login/login";

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  private password = {
    currentPassword : '',
    password : '',
    samePassword : ''
  };

  private loader;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService : AuthService,
    public loadingCtrl : LoadingController,
    public app : App) {
  }

  changePassword(){
    this.presentLoading();
    this.authService.changePassword(this.password)
      .subscribe(res => {
        this.navCtrl.pop();
        this.dismissLoading();
      },err => {
        this.dismissLoading();
        this.logout();
      });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Um momento..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }

  logout(){
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }

}
