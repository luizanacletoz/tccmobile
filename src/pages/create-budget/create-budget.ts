import { Component } from '@angular/core';
import {
  AlertController,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ToastController,
  ViewController
} from 'ionic-angular';
import {BudgetService} from "../../services/budget.service";
import { Keyboard } from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import {Diagnostic} from "@ionic-native/diagnostic";
import { AndroidPermissions } from '@ionic-native/android-permissions';


/**
 * Generated class for the CreateBudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-budget',
  templateUrl: 'create-budget.html',
})
export class CreateBudgetPage {

  private items = [];
  private item = {
    name : '',
    amount : ''
  };

  private lat;
  private long;
  private km;

  private loader;

  constructor(
    public navCtrl: NavController,
    public viewCtrl : ViewController,
    public toastCtrl : ToastController,
    public budgetService : BudgetService,
    private keyboard: Keyboard,
    public geolocation : Geolocation,
    public diagnostic : Diagnostic,
    public androidPermissions : AndroidPermissions,
    public loadingCtrl : LoadingController,
    public alertCtrl : AlertController) {
  }

  dismiss(){
    this.viewCtrl.dismiss({ completed : false });
  }

  addItem(){
    if(this.item.name == '' || this.item.amount == ''){
      this.presentToast('Informe todos os campos para adicionar o produto');
    }else{
      this.items.push({name : this.item.name, amount : this.item.amount});
      this.item.name = '';
      this.item.amount = '';
    }
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

  removeItem(index){
    this.items.splice(index, 1);
  }

  ionViewDidEnter(){
    let config = JSON.parse(localStorage.getItem("config"));
    this.km = config.rangeKm;
  }

  createBudget(){
    this.diagnostic.isGpsLocationEnabled()
      .then((state) => {

        if(state){

          let options = {
            enableHighAccuracy: false,
            timeout: 15000
          };
          this.presentLoading();
          this.geolocation.getCurrentPosition(options).then((res) => {
            this.lat = res.coords.latitude;
            this.long = res.coords.longitude;

            this.budgetService.createBudget({
              lat : this.lat,
              long : this.long,
              km : this.km,
              items : this.items
            }).subscribe(res => {
              this.dismissLoading();
              this.viewCtrl.dismiss({ id : res.id , completed : true});
            }, err => {
              this.presentToast('Erro interno do servidor');
              this.dismissLoading();
            });

          }, err => {

            console.log(err)
            this.dismissLoading();
          });
        }else{

          let confirm = this.alertCtrl.create({
            title: '<b>Localização</b>',
            message: 'A localização está desativada. Vá para as configurações e ative para prosseguir.',
            buttons: [
              {
                text: 'Cancelar',
                role: 'Cancel',
                handler: () => {
                  this.dismissLoading();
                }
              },
              {
                text: 'Ir para configurações',
                handler: () => {
                  this.diagnostic.switchToLocationSettings()
                }
              }
            ]
          });
          confirm.present();
        }
      }, () => {
        this.presentToast('A localização deve estar ativada');
      });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Um momento..."
    });
    this.loader.present();
  }

  dismissLoading(){
    this.loader.dismissAll();
  }
}
