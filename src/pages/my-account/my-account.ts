import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AuthService} from "../../services/auth.service";
import {LoginPage} from "../login/login";

/**
 * Generated class for the MyAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {

  private user;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService : AuthService,
    public app : App) {
  }

  ionViewWillEnter(){
    this.authService.info()
      .subscribe(res => {
        this.user = res;
      }, err => {
        this.logout();
      });
  }

  logout(){
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }
}
