import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

/**
 * Generated class for the SearchOptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-option',
  templateUrl: 'search-option.html',
})
export class SearchOptionPage {

  private searchOptions = {
    isOpen : true
  };

  constructor(public viewCtrl : ViewController) {
    let options = JSON.parse(localStorage.getItem('searchOptions'));
    if(options){
      this.searchOptions = options;
    }else{
      localStorage.setItem('searchOptions', JSON.stringify(this.searchOptions));
    }
  }

  dismiss(){
    localStorage.setItem('searchOptions', JSON.stringify(this.searchOptions));
    this.viewCtrl.dismiss(this.searchOptions);
  }

}
