import { extractData, handleError } from '../app/app.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '../app/app.interceptor';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class CompanyService {

  constructor(private _http: HttpClient) {
  }

  getById(id){
    return this._http.get(`companies/${id}`)
      .map(extractData)
      .catch(handleError);
  }
}
