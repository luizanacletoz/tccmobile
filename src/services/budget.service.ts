import { extractData, handleError } from '../app/app.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '../app/app.interceptor';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class BudgetService {

  constructor(private _http: HttpClient) {
  }

  toggle(id){

    return this._http.put(`budgets/${id}`, {});
  }

  getById(id){
    return this._http.get(`budgets/${id}`)
      .map(extractData)
      .catch(handleError);
  }

  getAll(isOpen = true){
    return this._http.get(`budgets?isOpen=${isOpen}`)
      .map(extractData)
      .catch(handleError);
  }

  createBudget(data){
    return this._http.post('budgets', data)
      .map(extractData)
      .catch(handleError);
  }
}
