import {Injectable} from "@angular/core";
import {HttpClient} from "../app/app.interceptor";
import {extractData, handleError} from "../app/app.config";

@Injectable()
export class ClientService {

  constructor(private _http: HttpClient) {
  }

  changeClientData(data){
    return this._http.put('accounts/clients', JSON.stringify(data))
  }

  register(data){
    return this._http.post('accounts/clients', JSON.stringify(data))
      .map(extractData)
      .catch(handleError);
  }
}
