import { extractData, handleError } from '../app/app.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '../app/app.interceptor';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class AuthService {

  constructor(private _http: HttpClient) {
  }

  info() {
    return this._http.get('accounts/info')
      .map(extractData)
      .catch(handleError)
  }

  auth(data){
    return this._http.post('accounts/auth', JSON.stringify(data))
      .map(extractData)
      .catch(handleError)
  }

  changePassword(data){
    return this._http.put('accounts/password', JSON.stringify(data))
  }
}
