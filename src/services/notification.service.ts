import { extractData, handleError } from '../app/app.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '../app/app.interceptor';
import 'rxjs/add/operator/map'
import 'rxjs/Rx';

@Injectable()
export class NotificationService {

  constructor(private _http: HttpClient) {
  }

  getAll(){
    return this._http.get(`notifications?`)
      .map(extractData)
      .catch(handleError);
  }

}
