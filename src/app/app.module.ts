import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {BudgetPage} from "../pages/budget/budget";
import {ConfigurationPage} from "../pages/configuration/configuration";
import {MyAccountPage} from "../pages/my-account/my-account";
import {LoginPage} from "../pages/login/login";
import {AuthService} from "../services/auth.service";
import {HttpClient} from "./app.interceptor";
import { HttpModule} from "@angular/http";
import {BudgetService} from "../services/budget.service";
import {BudgetDetailPage} from "../pages/budget-detail/budget-detail";
import {SearchOptionPage} from "../pages/search-option/search-option";
import {CreateBudgetPage} from "../pages/create-budget/create-budget";
import {NotificationPage} from "../pages/notification/notification";
import {NotificationService} from "../services/notification.service";
import {CompanyDetailPage} from '../pages/company-detail/company-detail';
import {CompanyService} from "../services/company.service";
import {LaunchNavigator} from "@ionic-native/launch-navigator";
import {Geolocation} from "@ionic-native/geolocation";
import {Diagnostic} from "@ionic-native/diagnostic";
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {ChangePasswordPage} from "../pages/change-password/change-password";
import {ChangePersonalDataPage} from "../pages/change-personal-data/change-personal-data";
import {ClientService} from "../services/client.service";
import {RegisterPage} from "../pages/register/register";


@NgModule({
  declarations: [
    MyApp,
    BudgetPage,
    ConfigurationPage,
    MyAccountPage,
    LoginPage,
    BudgetDetailPage,
    SearchOptionPage,
    CreateBudgetPage,
    NotificationPage,
    CompanyDetailPage,
    ChangePasswordPage,
    ChangePersonalDataPage,
    RegisterPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BudgetPage,
    ConfigurationPage,
    MyAccountPage,
    LoginPage,
    BudgetDetailPage,
    SearchOptionPage,
    CreateBudgetPage,
    NotificationPage,
    CompanyDetailPage,
    ChangePasswordPage,
    ChangePersonalDataPage,
    RegisterPage,
    TabsPage
  ],
  providers: [
    HttpClient,
    AuthService,
    BudgetService,
    NotificationService,
    CompanyService,
    ClientService,
    LaunchNavigator,
    Geolocation,
    Diagnostic,
    AndroidPermissions,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
