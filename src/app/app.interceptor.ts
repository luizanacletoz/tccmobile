import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { AppSettings } from './app.config';

@Injectable()
export class HttpClient {

  constructor(private http: Http) { }

  get(url) {
    return this.http.get(AppSettings.API_URL + url,
      this.getRequestOptionArgs()
    );
  }

  post(url, data) {
    return this.http.post(AppSettings.API_URL + url, data,
      this.getRequestOptionArgs()
    );
  }

  delete(url) {
    return this.http.delete(AppSettings.API_URL + url,
      this.getRequestOptionArgs()
    );
  }

  put(url, data = {}) {
    return this.http.put(AppSettings.API_URL + url, data,
      this.getRequestOptionArgs()
    );
  }

  getToken() {
    return JSON.parse(sessionStorage.getItem('token') || localStorage.getItem('token'))
  }

  private getRequestOptionArgs(): RequestOptionsArgs {
    let headers = new Headers();
    let options = new RequestOptions({ headers: headers });

    let token = this.getToken();

    headers.append('Content-Type', 'application/json');
    if(token != null)
      headers.append('Authorization', `Bearer ${token.accessToken}`);

    return options;
  }
}
