import { Observable } from "rxjs";
import { Response} from '@angular/http';

export class AppSettings {
  public static API_URL = 'https://orcafacil.azurewebsites.net/api/';
/*
  public static API_URL = 'https://localhost:5001/api/';
*/
}

export function extractData(res: Response) {
  return res.json();
}

export function handleError(error: Response | any) {
  error = JSON.parse(error._body);
  return Observable.throw(error);
}
